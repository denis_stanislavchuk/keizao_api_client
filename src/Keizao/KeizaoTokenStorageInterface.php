<?php

namespace Keizao;

/**
 * Interface KeizaoTokenStorageInterface
 * @package Keizao
 */
interface KeizaoTokenStorageInterface
{
    /**
     * Save access token.
     *
     * @param mixed $tokenStorage
     */
    public function set($tokenStorage);

    /**
     * Get access token from storage.
     *
     * @return array
     */
    public function get();

    /**
     * Delete access token from storage.
     */
    public function invalidate();
}
