<?php

namespace Keizao;

/**
 * Class KeizaoFileTokenStorage
 * @package Keizao
 */
class KeizaoFileTokenStorage implements KeizaoTokenStorageInterface
{
    /** @var string */
    private $file;

    /**
     * KeizaoFileTokenStorage constructor.
     */
    public function __construct($dir)
    {
        $this->file = $dir . '/storage.txt';
    }

    /**
     * {@inheritdoc}
     *
     * @param array $token
     * @return int
     */
    public function set($token)
    {
        $string = serialize($token);
        return file_put_contents($this->file, $string);
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed|null
     */
    public function get()
    {
        if (!file_exists($this->file)) {
            return null;
        }

        $data = unserialize(file_get_contents($this->file));

        if (empty($data)) {
            return null;
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function invalidate()
    {
        $this->set('');
    }
}