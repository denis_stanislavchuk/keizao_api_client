<?php

namespace Keizao;

/**
 * Class KeizaoApiClient
 *
 * Allows to interact with Keizao API
 *
 * @package Keizao
 */
class KeizaoApiClient
{
    /** @var string  */
    private $apiUrl = 'http://imei.iconxlab.com';

    /** @var string  */
    private $grantType = 'client_credentials';

    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var KeizaoTokenStorageInterface string */
    private $storage;

    /**
     * KeizaoApiClient constructor.
     *
     * @param $clientId
     * @param $clientSecret
     * @param KeizaoTokenStorageInterface $storage
     */
    public function __construct($clientId, $clientSecret, KeizaoTokenStorageInterface $storage)
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->storage      = $storage;
    }

    /**
     * Get access token.
     *
     * @return bool
     */
    private function issueAccessToken()
    {
        $token = $this->storage->get();

        if ($this->tokenIsExpired($token)) {
            $params = array(
                'client_id'     => $this->clientId,
                'client_secret' => $this->clientSecret,
                'grant_type'    => $this->grantType
            );
            $token = $this->request('/oauth/v2/token', $params);
            $token->expires = new \DateTime("+ {$token->expires_in} seconds");
            $this->storage->set($token);
            return $token;
        }

        return $token;
    }

    /**
     * Check token expiration.
     *
     * @return bool
     */
    private function tokenIsExpired($token)
    {
        if (empty($token)) {
            return false;
        }

        return (bool) $token->expires->diff(new \DateTime())->format('%r');
    }

    /**
     * Execute device query.
     *
     * @param string $imei
     * @return array
     */
    public function checkImei($imei)
    {
        $token = $this->issueAccessToken();

        return $this->request("/api/getblacklistinfos/{$imei}", ['access_token' => $token->access_token]);
    }

    /**
     * @param $url
     * @param array $params
     * @return bool|mixed
     */
    private function request($url, array $params) {
        $base = $this->apiUrl . $url;
        $postfields = http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        if (empty($response)) {
            return false;
        }

        return json_decode($response);
    }
}